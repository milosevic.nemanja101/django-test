# Introduction
Simple REST API with basic CRUD for Post, Upvote and Downvote resources

# Overview
Current API is based on tastypie library. Endpoints start with http://127.0.0.1:8000/api/post/

# Authentication
Current model uses default tastypie authentication, all relevant code could be found in api app of this project.

# Error Codes
401 Authentication error

# Rate limit
100 queries for user signup, limit due to emailhunter free plan

#About the task
Implemeneted features, user login signup (http://127.0.0.1:8000/signup)
Basic browsable rest framework, basic CRUD API, post models, emailhunter verification on sign up.

There was an attempt to implement JWT auth following the guide from auth0.com

https://auth0.com/docs/quickstart/backend/django/01-authorization

Defined public and private endpoints
	
	GET /api/public: available for non-authenticated requests
    GET /api/private: available for authenticated requests containing an Access Token with no additional scopes
    GET /api/private-scoped: available for authenticated requests containing an Access Token with the read:messages scope granted

Unfortunatelly there is some internal server error, upon an attempt to authenticate to api using bearer jwt token, wich i couldn't patch in reasonable time.

