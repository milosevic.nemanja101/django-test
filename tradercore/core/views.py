# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.views import APIView
# Create your views here.

from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

from django.contrib.auth.models import User, Group
from rest_framework import viewsets
#from core.serializers import UserSerializer, GroupSerializer

from rest_framework.permissions import AllowAny
from core.forms import SignUpForm
from pyhunter import PyHunter

@login_required
def home(request):
    return render(request, 'home.html')


HUNTER_API_KEY = 'b1366bdf49cb31630949c11a0ef5c3e31815df5a'

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid() and (valid_email(form.cleaned_data.get('email'))):
			form.save()
			username = form.cleaned_data.get('username')           
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)
			login(request, user)
			return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

def valid_email(email):
	hunter = PyHunter(HUNTER_API_KEY)
	if((hunter.email_verifier(email)['result'])=='deliverable'):
		return True
	else:
		return False




