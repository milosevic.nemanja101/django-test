"""tradercore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from core import views as core_views
from django.contrib.auth import views as auth_views
from django.conf.urls import include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from core import views
from django.conf.urls import url

from . import views
from api.resources import PostResource, UpvoteResource, DownvoteResource

upvote_resource = UpvoteResource()

downvote_resource = DownvoteResource()

post_resource = PostResource()


from django.conf.urls import url, include
from rest_framework import routers


router = routers.DefaultRouter()




urlpatterns = [
    url(r'^api/public$', views.public),
    url(r'^api/private$', views.private),
    url(r'^api/private-scoped$', views.private_scoped),

	url(r'^', include(router.urls)),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
    url(r'^signup/$', core_views.signup, name='signup'),
	url(r'^logout/$', auth_views.logout, {'next_page': 'login'}, name='logout'),
	url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='login'),
	url(r'^$', core_views.home, name='home'),
	url(r'api-token-auth/', obtain_jwt_token),
    url(r'api-token-refresh/', refresh_jwt_token),
	url(r'^api/', include(post_resource.urls)),

	
]



