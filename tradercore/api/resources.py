from tastypie.resources import ModelResource
from posts.models import Post, Upvote, Downvote
from tastypie.authorization import Authorization



class PostResource(ModelResource):
    class Meta:
		queryset = Post.objects.all()
		resource_name = 'post'
		authorization = Authorization()



class UpvoteResource(ModelResource):
    class Meta:
		queryset = Upvote.objects.all()
		resource_name = 'upvote'
		authorization = Authorization()


class DownvoteResource(ModelResource):
    class Meta:
		queryset = Downvote.objects.all()
		resource_name = 'downvote'
		authorization = Authorization()














