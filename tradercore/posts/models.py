# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

from django.db import models


class Post(models.Model):
	post_title = models.CharField(max_length=200, default = None)
	post_text = models.TextField()
	pub_date = models.DateTimeField('date published', auto_now=True)
	votes = models.IntegerField(default=0) 
	def __str__(self):
		return '%s %s' % (self.post_title, self.post_text)

class Upvote(models.Model):
	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	votes = models.IntegerField(default=0)
    


class Downvote(models.Model):
	post = models.ForeignKey(Post, on_delete=models.CASCADE)
	votes = models.IntegerField(default=0)
    
